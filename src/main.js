import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  // template: '<div>123</div>',
  render: h => h(App),
  // ...App
}).$mount('#app')